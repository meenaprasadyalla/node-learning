const weather = require('./weather/weather.js')
const request = require('request');
const yargs = require('yargs');
const geocode = require('./geocode/geocode.js');
const argv =  yargs
.options({
address: {
    demand: true,
    alias: 'a',
    describe: 'Address to fech weather for',
    string: true
}
})
.help()
.alias('help', 'h')
.argv

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
if (errorMessage){
    console.log(errorMessage)
}
else { 
    console.log(results.address);
    weather.getWeather(results.latitude,results.longitude, (errorMessage, weatherResults) => {
        if (errorMessage){
            console.log(errorMessage);
    
        }else {
            // console.log(JSON.stringify(weatherResults, undefined, 2));
             console.log(`It's currently ${weatherResults.temperature}. It feels like ${weatherResults.apparentTemperature}.`);
        }
    
    });
    

}
} );







