const yargs = require('yargs');
const axios = require('axios');
const argv =  yargs
.options({
address: {
    demand: true,
    alias: 'a',
    describe: 'Address to fech weather for',
    string: true
}
})
.help()
.alias('help', 'h')
.argv
var encodedAddress = encodeURIComponent(argv.address);
console.log(encodedAddress);

var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

console.log(geocodeUrl);
axios.get(geocodeUrl).then((response)=>{
    if(response.data.status === 'ZERO_RESULTS'){
        throw new Error('Unable to find that address.')
    }else if(response.data.status === 'OVER_QUERY_LIMIT'){
        throw new Error('Please try again.')
    }
    var lat = response.data.results[0].geometry.location.lat;
    var lng = response.data.results[0].geometry.location.lng;
    var weatherUrl = `https://api.darksky.net/forecast/1bc065e4ab7fd54c4dcb5f6b9add2e72/${lat},${lng}`
console.log(response.data.results[0].formatted_address);
return axios.get(weatherUrl).then((response)=>{
    var temperature = response.data.currently.temperature;
    var apparentTemperature = response.data.currently.apparentTemperature;
    console.log(`It's cureently  ${temperature}`);
    console.log(`Feels lika  ${apparentTemperature}`);
    
})
}).catch((e)=>{
    if(e.code === 'ENOTFOUND'){
        console.log('unable to connect to api servers');
    }else{
        console.log(e.message)
    }
 
})