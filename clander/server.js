console.log('started working server.js');
const router = require('koa-router')();
var koa = require('koa');
const koaPg = require ('koa-pg');
const bodyParser = require('koa-bodyparser');

var app = new koa();
// db init
var config = {
    name: 'calender_db',
    pg: {
        user: 'postgres',
        database: 'CALENDER',
        password: 'postgresql',
        port: 5432,
        max: 10,
        idleTimeoutMillis: 60
    }
}



app.use(koaPg(config))
app.use(bodyParser());
app.use (router.routes())
.use (router.allowedMethods());
app.listen(3000,()=>console.log('listening to port 3000'))

