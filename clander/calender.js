console.log('started working server.js');
const router = require('koa-router')();
var koa = require('koa');
const koaPg = require ('koa-pg');
const bodyParser = require('koa-bodyparser');

var app = new koa();

//CURD OPERATIONS WITH EMPLOYEE TABLE..

// db init
var config = {
    name: 'calender_db',
    pg: {
        user: 'postgres',
        database: 'CALENDER',
        password: 'postgresql',
        port: 5432,
        max: 10,
        idleTimeoutMillis: 60
    }
}
app.use(koaPg(config))
/// GETTING DATES FROM CALENDER.........
router.get('/calender' ,getCalender)
router.get('/nonworkingday' ,getNonWorkingDay)


//get all dates in a claneder
async function  getCalender(ctx,next)
{
    try{
    console.log ('GET /employee with ' + JSON.stringify(ctx.params));
    
    var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from calendar');
    console.log('result', result.rows)
    ctx.body=result.rows

} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}


//get all non-working days in a calender
async function getNonWorkingDay(ctx,next)
{
    try{
    console.log ('GET /nonworking day with ' + JSON.stringify(ctx.params));
    
    var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from calendar where day_of_week = $1 OR day_of_week = $2',[6,0]);
    console.log('result', result.rows)
    ctx.body=result.rows

} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}






app.use(bodyParser());
app.use (router.routes())
.use (router.allowedMethods());
app.listen(3000,()=>console.log('listening to port 3000'))

