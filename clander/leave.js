console.log('started working server.js');
const uuid = require ('node-uuid');
const router = require('koa-router')();
var koa = require('koa');
const koaPg = require ('koa-pg');
const bodyParser = require('koa-bodyparser');
var nodemailer = require('nodemailer');

var app = new koa();

//CURD OPERATIONS WITH EMPLOYEE TABLE..

// db init
var config = {
    name: 'calender_db',
    pg: {
        user: 'postgres',
        database: 'CALENDER',
        password: 'postgresql',
        port: 5432,
        max: 10,
        idleTimeoutMillis: 60
    }
}
app.use(koaPg(config))


router.get('/raiseLeave/:employee_name' ,raiseLeave)
router.get('/leaves' ,getLeaves)
router.get('/leave/:employee_name' ,getemployeeLeaves)
router.post('/leave' ,postLeave)
router.put('/leave/:leave_id' ,updateLeave)
router.delete('/leave/:leave_id' ,deleteLeave)

// Raise a new Leave 
async function raiseLeave(ctx,next){
    console.log("yes");
    var totalLeaves = 24;
   
    var res = await getemployeeLeaves(ctx,next);
    var usedLeaves = res.length
    if (usedLeaves <= totalLeaves){
console.log("mail is sender to HR")
console.log(`employee_name:${ctx.params.employee_name}`)

// Send a Mail to manager
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'suraaga.pvt@gmail.com',
    pass: 'suraaga@111'
  }
});

var mailOptions = {
  from: 'meenaprasadyalla@gmail.com',
  to:[ 'meenuyalla@gmail.com','vinod@suraaga.com'],
  attachments: [{filename: 'leave.txt',path: './leave.txt'
}],
  subject: `employee_name:${ctx.params.employee_name} Sending Email using Node.js leave application`,
  text: 'Sir i will be on leave for two days, that is 2018-03-16 and 2018-03-16(friday and monday) thanq sir'
};

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
});
    }else{
        console.log("U have crossed the leaves limit")
    }
    console.log("res",res)
    console.log("res-length",usedLeaves)

}


//get all leaves from leaves table
async function getLeaves(ctx,next)
{
try{
   console.log('get leaves is printing ')

   var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from leave')
   var leaveLength = result.rows.length
    console.log(result.rows);
    
   ctx.body=result.rows
   console.log(leaveLength)
   return result.rows;
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}




//get single employee leave from leave table
async function getemployeeLeaves(ctx,next)
{
    try {
    console.log ('GET /leave/employee_name with ' + JSON.stringify(ctx.params));
    
    var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from leave where employee_name = $1', [ctx.params.employee_name]);
    console.log('result', result.rows)
    ctx.body=result.rows
    var leaveLength = result.rows.length
    console.log(leaveLength)
    return result.rows
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}


 
// post a new leave in leave table
async function postLeave(ctx,next)
{
    try{
var data = ctx.request.body
data.leave_id = uuid.v1();
console.log(data.leave_id);
console.log(ctx.request.body)
var result = await ctx.pg.calender_db.client.queryPromise('INSERT into  leave(leave_id,employee_id,employee_name,leave_date,leave_reason) values($1,$2,$3,$4,$5)', [data.leave_id,data.employee_id,data.employee_name,data.leave_date,data.leave_reason]);
console.log(ctx.request.body)
ctx.body=ctx.request.body
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}



// update an existing  employee in employee table
async function updateLeave(ctx,next) 
{
    try{
   var data = ctx.request.body
   console.log(data);
    var result = await ctx.pg.calender_db.client.queryPromise('UPDATE leave set employee_id= $2, employee_name = $3,leave_date = $4,leave_reason = $5 where leave_id = $1', [ctx.params.leave_id,data.employee_id,data.employee_name,data.leave_date,data.leave_reason]);
   console.log(result);
   ctx.body=result;
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}


// delete an existing leave from leaves table
async function deleteLeave (ctx,next)
{
    try{
    console.log(ctx.params)
 var data = ctx.request.body
 console.log(data);
 
    var result = await ctx.pg.calender_db.client.queryPromise('DELETE from leave where leave_id = $1',[ctx.params.leave_id]);
    console.log(result);
    ctx.body=result

} catch(error){
    console.log(error)
    ctx.body = error.message
        }




}




app.use(bodyParser());
app.use (router.routes())
.use (router.allowedMethods());
app.listen(3000,()=>console.log('listening to port 3000'))