console.log('started working server.js');
const router = require('koa-router')();
var koa = require('koa');
const koaPg = require ('koa-pg');
const bodyParser = require('koa-bodyparser');

var app = new koa();

//CURD OPERATIONS WITH EMPLOYEE TABLE..

// db init
var config = {
    name: 'calender_db',
    pg: {
        user: 'postgres',
        database: 'CALENDER',
        password: 'postgresql',
        port: 5432,
        max: 10,
        idleTimeoutMillis: 60
    }
}
app.use(koaPg(config))





router.get('/employees' ,getEmployees)
router.get('/employee/:employee_id' ,getSingleEmployee)
router.post('/employee' ,postEmployee)
router.put('/employee/:employee_id' ,updateEmployee)
router.delete('/employee/:employee_id' ,deleteEmployee)



//get all employees from employee table
async function  getEmployees(ctx,next)
{
//    ctx.body = 'welcome to employees table'
   console.log('get employees is printing ')

try {
    var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from employee')
    console.log(result.rows);

    ctx.body=result.rows
} catch (error) {
    console.error(error)
    ctx.body = error.message
}
}

//get single employee from employee table
async function getSingleEmployee(ctx,next)
{
    console.log ('GET /employee with ' + JSON.stringify(ctx.params));
    try {
    // var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from employee where employee_id = $1', ['0001']);
    var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from employee where employee_id = $1', [ctx.params.employee_id]);
    console.log('result', result.rows)
    ctx.body=result.rows
    } catch(error){
console.log(error)
ctx.body = error.message
    }
}

 
// post a new employee to employee table
async function postEmployee(ctx,next)
{
    try{
var data = ctx.request.body
var result = await ctx.pg.calender_db.client.queryPromise('INSERT into  employee(employee_id,employee_name,employee_role,employee_email,employee_phone,joining_date) values($1,$2,$3,$4,$5,$6)', [data.employee_id,data.employee_name,data.employee_role,data.employee_email,data.employee_phone,data.joining_date]);
console.log(ctx.request.body)
ctx.body=ctx.request.body
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}

// update an existing  employee in employee table
async function updateEmployee(ctx,next) 
{
    try{
   var data = ctx.request.body
   console.log(data);
    var result = await ctx.pg.calender_db.client.queryPromise('UPDATE employee set employee_name = $2 where employee_id = $1', [ctx.params.employee_id, data.employee_name]);
   console.log(result);
   ctx.body=result;

} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}


// delete an existing bemployee from employee table
async function deleteEmployee(ctx,next)
{
    try {
 var data = ctx.request.body
 console.log(data);
 
    var result = await ctx.pg.calender_db.client.queryPromise('DELETE from employee where employee_id = $1',[ctx.params.employee_id]);
    console.log(result);
    ctx.body=result
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}


app.use(bodyParser());
app.use (router.routes())
.use (router.allowedMethods());
app.listen(3000,()=>console.log('listening to port 3000'))


