console.log('started working server.js');
const router = require('koa-router')();
var koa = require('koa');
const koaPg = require ('koa-pg');
const bodyParser = require('koa-bodyparser');

var app = new koa();

//CURD OPERATIONS WITH EMPLOYEE TABLE..

// db init
var config = {
    name: 'calender_db',
    pg: {
        user: 'postgres',
        database: 'CALENDER',
        password: 'postgresql',
        port: 5432,
        max: 10,
        idleTimeoutMillis: 60
    }
}
app.use(koaPg(config))


router.get('/holidays' ,getHoliday)
router.get('/holiday/:occasion' ,getOccasionHoliday)
router.post('/holiday' ,postHoliday)
router.put('/holiday/:occasion' ,updateHoliday)
router.delete('/holiday/:occasion' ,deleteHoliday)


//get all holidays from holiday table
async function getHoliday(ctx,next) 
{
    try{
   console.log('get employees is printing ')

   var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from holiday')
   console.log(result.rows);
   ctx.body=result.rows
} catch(error){
    console.log(error)
    ctx.body = error.message
        }

}

//get single holiday from holiday table
async function getOccasionHoliday(ctx,next)
{
    try{  
    var result = await ctx.pg.calender_db.client.queryPromise('SELECT * from holiday where occasion = $1', [ctx.params.occasion]);
    console.log('result', result.rows)
    ctx.body=result.rows
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}
 
// post a new holiday in holiday table
async function postHoliday(ctx,next)
{
try{ 
var data = ctx.request.body
console.log(ctx.request.body)
var result = await ctx.pg.calender_db.client.queryPromise('INSERT into  holiday(date,occasion,day,day_of_week) values($1,$2,$3,$4)', [data.date,data.occasion,data.day,data.day_of_week]);
console.log(ctx.request.body)
ctx.body=ctx.request.body
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}

// update an existing holiday in holiday table
async function updateHoliday(ctx,next)
{
    try{
   var data = ctx.request.body
   console.log(data);
    var result = await ctx.pg.calender_db.client.queryPromise('UPDATE holiday set date = $2 where occasion= $1', [ctx.params.occasion,data.date]);
   console.log(result);
   ctx.body=result;
} catch(error){
    console.log(error)
    ctx.body = error.message
        }
}

// delete an existing holiday from holiday table
async function deleteHoliday(ctx,next) 
{
    try{
    console.log(ctx.params)
 var data = ctx.request.body
 console.log(data);
 
    var result = await ctx.pg.calender_db.client.queryPromise('DELETE from holiday where occasion = $1',[ctx.params.occasion]);
    console.log(result);
    ctx.body=result


} catch(error){
    console.log(error)
    ctx.body = error.message
        }



}




app.use(bodyParser());
app.use (router.routes())
.use (router.allowedMethods());
app.listen(3000,()=>console.log('listening to port 3000'))


