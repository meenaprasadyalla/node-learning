console.log('server.js is working..!');
const koaPg = require ('koa-pg');
const koa = require('koa');
const bodyParser = require('koa-bodyparser');
const uuid = require ('node-uuid');
const router = require('koa-router')();



const app = new koa ();

// db init
var config = {
    name: 'employees_db',
    pg: {
        user: 'postgres',
        database: 'employees',
        password: 'postgresql',
        port: 5432,
        max: 10,
        idleTimeoutMillis: 60
    }
}
app.use(koaPg(config))

router.get('/employees',async (ctx, next)=>{
    // ctx.body = "helloWorld"
    // console.log(`Printing..${ctx.body}`);
    console.log ('GET /employee with ' + JSON.stringify(ctx.params));
    var result = await ctx.pg.employees_db.client.queryPromise('SELECT * from employees')
    console.log('result', result.rows)
    ctx.body = result.rows;
} );

const getEmployee =  async (ctx, next) => {
    
    console.log(`Printing..${ctx.body}`);
    console.log ('GET /employee with ' + JSON.stringify(ctx.params));
    var result = await ctx.pg.employees_db.client.queryPromise('SELECT * from employees where employee_id = $1', [ctx.params.employee_id]);
    console.log('result', result.rows)
    ctx.body = result.rows;
}
router.get('/employee/:employee_id',getEmployee);




 router.post('/employee', async (ctx, next) => {


    console.log ('POST /employee (create) with body ' + JSON.stringify(ctx.request.body));
    var data = ctx.request.body;
    data.employee_id = uuid.v1();
    console.log(data.employee_id);
    var result = await ctx.pg.employees_db.client.queryPromise('INSERT into employees (employee_id, employee_name ) values ($1, $2)', [data.employee_id, data.employee_name]);
    console.log('result:', JSON.stringify(result));
    ctx.body = result;
    console.log(`Printing..${ctx.request.body}`);
    console.log(`Printing..${ctx.request.body.employee_name}`);
} );



 router.put('/employee/:employee_id', async (ctx, next) => {

     var data = ctx.request.body;
     console.log(data);
     
     var result = await ctx.pg.employees_db.client.queryPromise('UPDATE employees set employee_name = $2 where employee_id = $1', [ctx.params.employee_id, data.employee_name]);
     console.log('result:', JSON.stringify(result));
     ctx.body = result;
   
    console.log(`Printing..${ctx.body}`);
});
 router.delete('/employee/:employee_id', async (ctx, next) => {
    //  var data = ctx.request.body;
    //  console.log(data);
     var result = await ctx.pg.employees_db.client.queryPromise('DELETE from employees where employee_id = $1',[ctx.params.employee_id]);
      console.log('result:', result);
      var res = JSON.stringify(ctx.response);
      console.log(res);
     ctx.body = `deleteEmployee ${res}`;
    // console.log(`Printing..${ctx.body}`);
});

app.use(bodyParser());
app.use (router.routes())
.use (router.allowedMethods());
app.listen(3000,()=>{console.log("Server is listening on 3000")});

// {

      
//     "employee_name": "Meenaprasad"                                        
// }
