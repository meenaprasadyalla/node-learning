console.log('async-await wrkng');

//Async/await is a new way to write asynchronous code. Previous options for asynchronous code are callbacks and promises.
//Async/await makes asynchronous code look and behave a little more like synchronous code. This is where all its power lies.
//async await basics


//Noramal Function
const   normalFunction = () => {

    return 'normalFunction'
};
console.log(normalFunction());

 //Noraml promise function


 const normalpromiseFunction = () =>{
     return new Promise((resolve, reject) => {
         resolve('normalpromiseFunction')
         reject('This is an error')
     } )
 }
 console.log(normalpromiseFunction());

// //Async function

 const   asyncFunction = async () => {
  return 'asyncFunction'
};
 console.log(asyncFunction());
// // we are getting the vresult wrapped in a promise, so we can use then 
// //The value u returned will get resolved, using async function and we get the return value as a result mauy be string if it is string, may be boolean if it is boolean etc
  asyncFunction().then((name) => {
      console.log(name);
  })

// //Async function for rejecting

 const   asyncrejectFunction = async () => {
     throw new Error('This is an error');
         return 'Meenaprasad'
     };
    //console.log(asyncrejectFunction());
    // we are getting the promise, so we can use then 
     //The value u returned will get resolved, using async function and we get the return value as a result mauy be string if it is string, may be boolean if it is boolean etc
      asyncrejectFunction().then((name) => {
          console.log(name);
      }).catch((error) => {
  console.log('error');
      })

// //we used only half feature and we will use await 

 const asyncawaitFunction = async () => {
    var assamFunction =  (a, b) => a * b === 4
    var aboveName =  await assamFunction(2, 2);
   
      return aboveName;
    
   
 }
 console.log(asyncawaitFunction());
 asyncawaitFunction().then((name) => {
 console.log(`aysnc-await function returns - ${name}`);
 }).catch((error) => {
     console.log('error');
         });


// const arrayData =  (id) => {


// const array = [{
// 'id': 1,
// 'age': 22,
// 'name': 'meenaprasad'
// },
// {
//     'id': 2,
//     'age': 21,
//     'name': 'mp'
// }]
// var eachItem =  array.map((x,i) => {
// if(x.id == id)
// {
// //console.log(x);
// return x;
// }
// }
// )
// return eachItem;
// };



// const assyncDataFunction = async (x) => {
//     var data = await arrayData(x);

//     return data;
// console.log(data);

// }
// assyncDataFunction(1)
// .then((data) => {
// console.log(`...........${data}`);
// }).catch((error) => {
//     console.log(`assyncDataFunction error`);
// })
// /////////////////// A Real Life Example

// // const axios = require('axios');
// // const getExchange = (from, to) => {
// // return axios.get(`http://api.fixer.io/latest?base=&{from}`).then((response)=>{
// // return response.data.rates[to];
// //     }) 
// // }

// // getExchange('USD', 'CAD').then((rate)=>{
// //     console.log(rate);
// //     console.log('Meenaprasad')
// // })