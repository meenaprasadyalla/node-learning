const users = [{
    id: 1,
    name: 'Mp',
    schoolId: 101
},{
id: 2,
name: 'Pp',
schoolId: 999
}];
const grades = [{
    id: 1,
    schoolId: 101,
    grade: 89
},
{
    id: 2,
    schoolId: 999,
    grade: 100
},
{
    id: 3,
    schoolId: 101,
    grade: 80
}];
const getUser = (id) => {
    return new Promise((resolve, reject)=>{
        const user = users.find((user) => user.id === id);
        if (user) {
            resolve(user);
        }else {
            reject ('unable to find');
        }
    })

};
const getGrades = (schoolId) => {
return new Promise ((resolve, reject)=>{
resolve(grades.filter((grade) => grade.schoolId === schoolId))
})

}
getGrades(101).then((grade)=>{
console.log(grade);
}).catch((e)=>{
console.log(e);
})