const Koa = require('koa');
const app = new Koa();

// x-response-time

app.use(async (ctx, next) => {
    console.log('-1');
  const start = Date.now();
  console.log('-1',start);
  await next();
  console.log('1');
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// logger

app.use(async (ctx, next) => {
    console.log('-2');
  const start = Date.now();
  console.log('-2',start);
  await next();
  console.log('2');
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

// response

app.use(async ctx => {
    console.log('-3');
  ctx.body = 'Hello World';
  console.log('3');
  console.log('hello world');
});

app.listen(3000,()=>{console.log("Server 3000")});