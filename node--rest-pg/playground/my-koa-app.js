console.log('my-koa-app.js');
const koa = require('koa');
const app = new koa();
app.use((ctx) => {
    ctx.body = "hello world";
});
app.listen(3000, ()=>{
    console.log('port is listening on 3000');
});