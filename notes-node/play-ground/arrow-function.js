console.log('arrow-function')
var square = (x) => x * x;
console.log(square(2));
var user = {
    name: 'meenaprasad',
    sayHI: () => {
        console.log(`hi ${this.name}`);
    },
    sayHiAlt() {
        console.log(`hi ${this.name}`);
    }
}
user.sayHiAlt();