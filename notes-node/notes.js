console.log("notes.js..!")
const fs = require('fs');
var fetchNotes = () => {
    try 
    {
        var noteString = fs.readFileSync('notes-data.json');
        notes = JSON.parse(noteString);
        return notes;
    }
    catch (e) {
return [];
    }
    
}
var saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
}
var addNote = (title, body) => {
    var notes = fetchNotes();
    var note = {
        title,
        body
    };

var duplicateNotes = notes.filter((note)=> note.title === title)
if (duplicateNotes.length == 0)
{
   notes.push(note);
   saveNotes(notes);  
}
else {
    console.log('already exists');
}
}
var array = fetchNotes();
var getAll = () => {
    console.log('getting all notes');
    return fetchNotes();
}

var getNote = (title) => {
    var notes = fetchNotes();
    var note = {
        title
    };
var duplicateNotes = notes.filter((note)=> note.title === title)

    if ((notes.length > 0) && (duplicateNotes.length > 0)){
  
console.log('title' ,JSON.stringify(duplicateNotes));
    }
    
}
var removeNote = (title) => {
    var notes = fetchNotes();
    var note = {
        title
    };
    var duplicateNotes = notes.filter((note)=> note.title === title)
    if ((notes.length > 0) && (duplicateNotes.length !== 0))
    {
        var index = notes.indexOf(title);
        console.log('index', index);
        console.log('removing-notes', title);
        var deletedNotes = notes.splice(index, 1);
        console.log('deleted notes' ,deletedNotes);
        console.log('notes' ,notes);
        saveNotes(notes);

        
        
    }
    else{
        console.log('No such item in an array...!')
    }
    

}
//console.log(module);
// module.exports.addNote = () => {
// console.log('addNote');
// return 'New Note';
// };
// module.exports.add = (a, b) => a + b;
module.exports = {
    addNote,
    getAll,
    getNote,
    removeNote
}