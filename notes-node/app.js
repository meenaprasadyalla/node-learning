console.log("app.js..!")
const fs = require('fs');
const os = require('os');
const yargs = require('yargs');
const notes = require('./notes.js');
const titleOptions = {
    describe: 'Title of the note',
    demand: true,
    alias: 't'
};
const bodyOptions = {
    describe: 'Body of the note',
    demand: true,
    alias: 'b'
};
const argv = yargs

.command('add', 'Add a new note',  {
    title: titleOptions,
    body: bodyOptions
})
.command('list', 'list all notes')
.command('remove', 'Remove a note',  {
    title: titleOptions

})
.command('fetch', 'Fetch a note',  {
    title: titleOptions

})
.help()
.argv;
var command = process.argv[2];

// console.log('Command:', command);
// console.log('process' ,process.argv)
// console.log('Yargs' , argv)
if(command == 'add'){
    console.log('adding new')
    notes.addNote(argv.title, argv.body);
}
else if (command == 'list'){
var allNotes = notes.getAll();
console.log(`Listing all ${allNotes.length} notes.`);
allNotes.map((eachNote) => console.log(eachNote)); 
    
}
else if (command == 'remove'){
    notes.removeNote(argv.title);
    
}
else if (command == 'fetch'){
    notes.getNote(argv.title)
    console.log('Fetching Nodes');
}
else
{
    console.log('no command..!')
}